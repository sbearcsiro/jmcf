/**
 *
 */
package com.innovalog.jmcf.fields;

import bsh.EvalError;
import bsh.Interpreter;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.DatePickerConverter;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.DateField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.googlecode.jsu.util.WorkflowUtils;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Map;

/**
 * @author david
 */
public class CalculatedDateField extends AbstractCalculatedFormulaField<Date> implements DateField {
  private DatePickerConverter dateConverter;
  private final DateTimeFormatter datePickerFormatter;

  public CalculatedDateField(WorkflowUtils workflowUtils, DatePickerConverter dateConverter, DateTimeFormatter datePickerFormatter) {
    super(workflowUtils);
    this.dateConverter = dateConverter;
    this.datePickerFormatter = datePickerFormatter;
  }

  /*
    * (non-Javadoc)
    *
    * @see com.atlassian.jira.issue.customfields.CustomFieldType#
    * getSingularObjectFromString(java.lang.String)
    */
  public Date getSingularObjectFromString(String string) throws FieldValidationException {
    return dateConverter.getTimestamp(string);
  }

  /*
    * (non-Javadoc)
    *
    * @see com.atlassian.jira.issue.customfields.CustomFieldType#
    * getStringFromSingularObject(java.lang.Object)
    */
  public String getStringFromSingularObject(Date singularObject) {
    assertObjectImplementsType(Date.class, singularObject);
    return dateConverter.getString((Date) singularObject);
  }

  @Override
  protected Date convertResult(Object o) {
    if (o instanceof Date)
      return (Date) o;

    log.error("CalculatedDateField: formula does did return a Date object: " + o.toString());
    return null;
  }

  @Override
  public Map<String, Object> getVelocityParameters(final Issue issue, final CustomField field, final FieldLayoutItem fieldLayoutItem) {
    final Map<String, Object> map = super.getVelocityParameters(issue, field, fieldLayoutItem);
    map.put("dateTimePicker", Boolean.TRUE);
    map.put("datePickerFormatter", datePickerFormatter);
    map.put("dateFieldFormat", datePickerFormatter.withStyle(DateTimeStyle.DATE));
    map.put("titleFormatter", datePickerFormatter.withStyle(DateTimeStyle.COMPLETE));
    map.put("iso8601Formatter", datePickerFormatter.withStyle(DateTimeStyle.ISO_8601_DATE_TIME));

    return map;
  }

}
