/**
 *
 */
package com.innovalog.jmcf.fields;

import bsh.EvalError;
import bsh.Interpreter;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.googlecode.jsu.util.WorkflowUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * @author david
 */
public class CalculatedTextField extends AbstractCalculatedFormulaField<String> {
  public CalculatedTextField(WorkflowUtils workflowUtils) {
    super(workflowUtils);
  }

  @Override
  protected String convertResult(Object o) {
    return o.toString();
  }

  /*
    * (non-Javadoc)
    *
    * @see com.atlassian.jira.issue.customfields.CustomFieldType#
    * getSingularObjectFromString(java.lang.String)
    */
  public String getSingularObjectFromString(String string) throws FieldValidationException {
    return string;
  }

  /*
    * (non-Javadoc)
    *
    * @see com.atlassian.jira.issue.customfields.CustomFieldType#
    * getStringFromSingularObject(java.lang.Object)
    */
  public String getStringFromSingularObject(String singularObject) {
    return singularObject;
  }
}
