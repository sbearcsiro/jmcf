package com.innovalog.jmcf.fields;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.StringConverterImpl;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.status.Status;

/**
 * User: david.fischer
 * Date: 17/10/11
 * Time: 14:16
 */
public class ParentIssueStatusCalculatedField extends CalculatedCFType {
    private final ConstantsManager constantsManager;

    public ParentIssueStatusCalculatedField(ConstantsManager constantsManager) {
        this.constantsManager = constantsManager;
    }

    public String getStringFromSingularObject(Object value) {
        assertObjectImplementsType(String.class, value);
        return StringConverterImpl.convertNullToEmpty((String) value);
    }

    public Object getSingularObjectFromString(String string) throws FieldValidationException {
        return string;
    }

    public Object getValueFromIssue(CustomField field, Issue issue) {
        assert issue!= null;

        Issue parent = issue.getParentObject();
        if (parent == null)
            return null;

        return parent.getStatusObject().getNameTranslation();
    }
}
