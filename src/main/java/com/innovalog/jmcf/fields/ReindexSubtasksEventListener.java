package com.innovalog.jmcf.fields;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.util.ImportUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.util.Collection;

/**
 *
 */
public class ReindexSubtasksEventListener implements DisposableBean, InitializingBean {
  private final EventPublisher eventPublisher;
  private final IssueIndexManager issueIndexManager;
  private Logger log = LoggerFactory.getLogger(ReindexSubtasksEventListener.class);

  private boolean listenerIsRegistered = false;

  public ReindexSubtasksEventListener(EventPublisher eventPublisher, IssueIndexManager issueIndexManager) {
    this.eventPublisher = eventPublisher;
    this.issueIndexManager = issueIndexManager;
  }

  void register() {
    if (!listenerIsRegistered) {
      eventPublisher.register(this);
      listenerIsRegistered = true;
    }
  }

  void unregister() {
    if (listenerIsRegistered) {
      eventPublisher.unregister(this);
      listenerIsRegistered = false;
    }
  }

  public void destroy() throws Exception {
    unregister();
  }

  @EventListener
  public void onIssueEvent(IssueEvent issueEvent) {
    if (issueEvent.getEventTypeId().equals(EventType.ISSUE_RESOLVED_ID)
      || issueEvent.getEventTypeId().equals(EventType.ISSUE_CLOSED_ID)
      || issueEvent.getEventTypeId().equals(EventType.ISSUE_REOPENED_ID)
      || issueEvent.getEventTypeId().equals(EventType.ISSUE_WORKSTARTED_ID)
      || issueEvent.getEventTypeId().equals(EventType.ISSUE_WORKSTOPPED_ID)
      || issueEvent.getEventTypeId().equals(EventType.ISSUE_GENERICEVENT_ID)
      || issueEvent.getEventTypeId().compareTo(EventType.ISSUE_WORKLOG_DELETED_ID) > 0) {

      Issue issue = issueEvent.getIssue();
      Collection<Issue> coll = issue.getSubTaskObjects();
      if (coll.size() > 0) {
        //let's reindex our subtasks
        boolean wasIndexing = ImportUtils.isIndexIssues();
        ImportUtils.setIndexIssues(true);
        try {
          issueIndexManager.reIndexIssueObjects(coll);
        } catch (IndexException e) {
          log.error("Error while re-indexing subtsks of issue " + issue.toString());
        } finally {
          ImportUtils.setIndexIssues(wasIndexing);
        }
      }
    }
  }

  public void afterPropertiesSet() throws Exception {
    register();
  }
}
