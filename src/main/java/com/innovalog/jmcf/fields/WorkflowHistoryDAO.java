package com.innovalog.jmcf.fields;

import com.atlassian.jira.issue.Issue;

import java.util.Collection;
import java.util.Map;

/**
 *
 */
public interface WorkflowHistoryDAO {
  Map findWorkflowEntry(Issue issue, Collection<Long> actionIds);
}
