package com.innovalog.jmcf.fields;

import com.atlassian.jira.ManagerFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.util.lang.Pair;
import com.atlassian.jira.workflow.WorkflowException;
import com.atlassian.jira.workflow.WorkflowManager;
import com.opensymphony.workflow.StoreException;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.spi.SimpleStep;
import com.opensymphony.workflow.spi.WorkflowStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public abstract class AbstractTransitionSearchField extends CalculatedCFType {
  private static Logger log = LoggerFactory.getLogger(AbstractTransitionSearchField.class);

  protected final WorkflowManager workflowManager;

  private static final String TRANSITION_ID_MARKER = "@TransitionId:";
  private static final String TRANSITION_NAME_MARKER = "@TransitionName:";
  private static final String WHICH_EXECUTION_MARKER = "@Execution:";
  private static final String EXECUTION_FIRST = "first";
  private static final String EXECUTION_LAST = "last";

  public AbstractTransitionSearchField(WorkflowManager workflowManager) {
    this.workflowManager = workflowManager;
  }

  protected boolean useFirstExecution(CustomField customfield, Issue issue) {
    if (customfield.getRelevantConfig(issue) != null) {
      String description = customfield.getRelevantConfig(issue).getCustomField().getDescription();
      if (description == null) {
        return false;
      }
      int start = description.indexOf("<!--");
      int end = description.indexOf("-->", start);
      while (start >= 0 && end >= 0) {
        int pos = description.indexOf(WHICH_EXECUTION_MARKER, start);
        if (pos >= 0 && pos < end) {
          return description.substring(pos + WHICH_EXECUTION_MARKER.length(), end).trim().equalsIgnoreCase(EXECUTION_FIRST);
        }
        start = description.indexOf("<!--", end);
        end = description.indexOf("-->", start);
      }
    }
    return false;
  }

  protected Collection<Long> getActionIDs(CustomField customfield, Issue issue) {
    if (customfield.getRelevantConfig(issue) != null) {
      //get the transition name or id
      String description = customfield.getRelevantConfig(issue).getCustomField().getDescription();
      if (description == null) {
        log.warn(this.getClass().getName() + ": could not find transition name or ID in custom field description");
        return null;
      }
      String transitionIdString = null, transitionName = null;
      int start = description.indexOf("<!--");
      int end = description.indexOf("-->", start);
      while (start >= 0 && end >= 0) {
        int pos = description.indexOf(TRANSITION_ID_MARKER, start);
        if (pos >= 0 && pos < end) {
          transitionIdString = description.substring(pos + TRANSITION_ID_MARKER.length(), end).trim();
        }
        pos = description.indexOf(TRANSITION_NAME_MARKER, start);
        if (pos >= 0 && pos < end) {
          transitionName = description.substring(pos + TRANSITION_NAME_MARKER.length(), end).trim().toLowerCase();
        }
        start = description.indexOf("<!--", end);
        end = description.indexOf("-->", start);
      }

      if (transitionIdString != null) {
        try {
          Set<Long> collection = new HashSet<Long>();
          for (String s : transitionIdString.split(","))
            collection.add(Long.decode(s.trim()));
          return collection;
        } catch (NumberFormatException e) {
          log.warn(this.getClass().getName() + ": invalid transition ID in custom field description");
          return null;
        }
      }
      if (transitionName == null) {
        log.warn(this.getClass().getName() + ": could not find transition name or ID in custom field description");
        return null;
      }

      //iterate through all actions to find corresponding transition
      try {
        Collection<ActionDescriptor> actions = workflowManager.getWorkflow(issue).getAllActions();
        Set<Long> collection = new HashSet<Long>();
        for (ActionDescriptor actionDescriptor : actions)
          if (actionDescriptor.getName().toLowerCase().equals(transitionName))
            collection.add(new Long(actionDescriptor.getId()));
        return collection;
      } catch (WorkflowException e) {
        log.error(e.getMessage());
        return null;
      }
    }

    return null;
  }

  /*protected Pair<String, Date> getHistoryStep(CustomField customfield, Issue issue) {
    WorkflowStore wfStore = null;
    try {
      wfStore = workflowManager.getStore();
      List<SimpleStep> historyActions = (List<SimpleStep>) wfStore.findHistorySteps(issue.getWorkflowId().longValue());

      Date maxDate = null;
      String userName = null;
      Long actionId = getActionIDs(customfield, issue);
      if (actionId == null)
        return null;
      for (SimpleStep historyAction : historyActions) {
        if (historyAction.getActionId() == actionId.intValue())
          if (maxDate == null || historyAction.getFinishDate().after(maxDate)) {
            maxDate = historyAction.getFinishDate();
            userName = historyAction.getCaller();
          }
      }
      return Pair.nicePairOf(userName, maxDate);
    } catch (StoreException e) {
      return null;
    }
  }
  */
}
