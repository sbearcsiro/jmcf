/**
 *
 */
package com.innovalog.jmcf.fields;

import java.util.Map;

import com.googlecode.jsu.util.WorkflowUtils;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.commons.pool.impl.GenericObjectPoolFactory;

import bsh.EvalError;
import bsh.Interpreter;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.util.velocity.NumberTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author david
 */
@SuppressWarnings("unchecked")
public class CalculatedNumberField extends AbstractCalculatedFormulaField<Double> {
  private static final String FORMATTER_MARKER = "@@Format:";

  public CalculatedNumberField(WorkflowUtils workflowUtils) {
    super(workflowUtils);
  }

  /*
    * (non-Javadoc)
    *
    * @see com.atlassian.jira.issue.customfields.CustomFieldType#
    * getSingularObjectFromString(java.lang.String)
    */
  public Double getSingularObjectFromString(String string) throws FieldValidationException {
    if (string == null)
      return null;
    try {
      return new Double(string);
    } catch (NumberFormatException e) {
      throw new FieldValidationException(e.getMessage());
    }
  }

  /*
    * (non-Javadoc)
    *
    * @see com.atlassian.jira.issue.customfields.CustomFieldType#
    * getStringFromSingularObject(java.lang.Object)
    */
  public String getStringFromSingularObject(Double singularObject) {
    if (singularObject instanceof Double)
      return singularObject.toString();
    else
      return null;
  }

  @Override
  protected Double convertResult(Object o) {
    try {
      return new Double(o.toString());
    } catch (NumberFormatException e) {
      log.error(className() + ": error evaluating formula: " + e.getMessage());
      return null;
    }
  }

  @Override
  public Map<String, Object> getVelocityParameters(final Issue issue, final CustomField field, final FieldLayoutItem fieldLayoutItem) {
    final Map<String, Object> map = super.getVelocityParameters(issue, field, fieldLayoutItem);
    NumberTool numberTool = new NumberTool(getI18nBean().getLocale());

    // get the formatter from the CustomField
    String desc = field.getDescription();
    if (desc != null) {
      String formatter = null;
      int start = desc.indexOf("<!--");
      int end = desc.indexOf("-->", start);
      while (start >= 0 && end >= 0) {
        int formatterPos = desc.indexOf(FORMATTER_MARKER, start);
        if (formatterPos >= 0 && formatterPos < end) {
          formatter = desc.substring(formatterPos + FORMATTER_MARKER.length(), end);
          break;
        } else {
          start = desc.indexOf("<!--", end);
          end = desc.indexOf("-->", start);
        }
      }

      if (formatter != null) {
        map.put("formatTool", new BshFormatter(formatter, numberTool));
      }
    }
    map.put("numberTool", numberTool);
    return map;
  }

  public class BshFormatter {
    private String formatScript;
    private final NumberTool numberTool;

    public BshFormatter(String formatterCode, NumberTool numberTool) {
      this.formatScript = formatterCode;
      this.numberTool = numberTool;
    }

    public String format(Object value) {
      if (value == null)
        return null;

      Interpreter bsh = new Interpreter();
      try {
        bsh.set("value", value);
        bsh.set("numberTool", numberTool);
        Object result = bsh.eval(formatScript);
        if (result == null)
          return null;
        return result.toString();
      } catch (EvalError evalError) {
        return evalError.getMessage();
      }
    }
  }
}
