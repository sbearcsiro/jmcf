/**
 * 
 */
package com.innovalog.jmcf.fields;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.Field;
import com.googlecode.jsu.util.WorkflowUtils;

/**
 * @author david
 *
 */
public final class IssueProxy {
	private Issue issue;
    private final WorkflowUtils workflowUtils;
	
	public IssueProxy(Issue i, WorkflowUtils workflowUtils) {
		issue = i;
        this.workflowUtils = workflowUtils;
    }
	
	public Object get(String fieldName)
	{
		Field field = workflowUtils.getFieldFromKey(fieldName);
		return workflowUtils.getFieldValueFromIssue(issue, field,false);
	}

  public Issue getIssueObject() {
    return issue;
  }
}
