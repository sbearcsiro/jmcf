package com.innovalog.jmcf.fields;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.usercompatibility.UserCompatibilityHelper;
import com.googlecode.jsu.util.WorkflowUtils;
import org.ofbiz.core.entity.jdbc.SQLProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class WorkflowFirstHistoryDAO implements WorkflowHistoryDAO {
  private static Logger log = LoggerFactory.getLogger(WorkflowFirstHistoryDAO.class);

  public static final String CALLER_KEY = "caller";

  public static final String FINISHDATE_KEY = "finishdate";

  String ENTITY_DS = "defaultDS";

  private final boolean withFormerWorkflows;
  private final WorkflowUtils workflowUtils;


  private final String selectStatement = "SELECT caller,finish_date FROM jiraissue issue "
    + "INNER JOIN OS_HISTORYSTEP workflow on issue.workflow_id = workflow.entry_id "
    + "WHERE issue.id = ? and workflow.action_id IN (%s) " + "ORDER BY finish_date ASC";

  private final String formerWorkflowsStatement =
    "SELECT ci.OLDVALUE, cg.CREATED FROM changeitem ci INNER JOIN changegroup cg " +
      "ON ci.groupid=cg.ID WHERE cg.issueid=? AND ci.field='workflow' " +
      "ORDER BY CREATED ASC";

  private final String selectStatement2 = "SELECT caller,finish_date FROM OS_HISTORYSTEP workflow  "
    + "WHERE entry_id = ? and action_id IN (%s) " + "ORDER BY finish_date ASC";


  public WorkflowFirstHistoryDAO(boolean withFormerWorkflows, WorkflowUtils workflowUtils) {
    this.withFormerWorkflows = withFormerWorkflows;
    this.workflowUtils = workflowUtils;
  }

  public Map findWorkflowEntry(Issue issue, Collection<Long> actionIds) {
    Map workflowEntry = new HashMap();
    SQLProcessor sqlProcessor = null;
    ResultSet resultSet = null;
    StringBuilder sb = new StringBuilder();
    Iterator<Long> iterator = actionIds.iterator();
    sb.append(iterator.next().longValue());
    while (iterator.hasNext()) {
      sb.append(" , ");
      sb.append(iterator.next().longValue());
    }
    String actionIdsAsString = sb.toString();
    try {
      sqlProcessor = new SQLProcessor(ENTITY_DS);
      if (withFormerWorkflows) {
        //we need to look into former workflows if any
        sqlProcessor.prepareStatement(formerWorkflowsStatement);
        sqlProcessor.setValue(issue.getId());
        sqlProcessor.executeQuery();
        resultSet = sqlProcessor.getResultSet();
        Set<String> workflows = new HashSet<String>();
        while (resultSet.next())
          workflows.add(resultSet.getString(1));

        for (String workflowId : workflows) {
          sqlProcessor.prepareStatement(String.format(selectStatement2, actionIdsAsString));
          sqlProcessor.setValue(workflowId);
          sqlProcessor.executeQuery();
          resultSet = sqlProcessor.getResultSet();
          if (resultSet.next()) {
            String userName = resultSet.getString(1);
            Timestamp finishDate = resultSet.getTimestamp(2);
            Object caller = workflowUtils.convertValueToAppUser(userName);
            workflowEntry.put(CALLER_KEY, caller);
            workflowEntry.put(FINISHDATE_KEY, finishDate);
            return workflowEntry;
          }
        }
      }

      sqlProcessor.prepareStatement(String.format(selectStatement, actionIdsAsString));
      sqlProcessor.setValue(issue.getId());
      sqlProcessor.executeQuery();
      resultSet = sqlProcessor.getResultSet();
      // Just the last entry if a transition was executed multiple times
      if (resultSet.next()) {
        String userName = resultSet.getString(1);
        Timestamp finishDate = resultSet.getTimestamp(2);
        Object caller = workflowUtils.convertValueToAppUser(userName);
        workflowEntry.put(CALLER_KEY, caller);
        workflowEntry.put(FINISHDATE_KEY, finishDate);
        return workflowEntry;
      }
    } catch (Throwable e) {
      log.error(e.getMessage(), e);
    } finally {
      if (sqlProcessor != null) {
        try {
          sqlProcessor.close();
        } catch (Exception e) {
          log.error(e.getMessage(), e);
        }
      }
    }
    return workflowEntry;
  }
}
