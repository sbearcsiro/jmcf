package com.innovalog.jmcf.fields;

import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.DatePickerConverter;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.DateField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.workflow.WorkflowManager;
import com.googlecode.jsu.util.WorkflowUtils;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

public class ActionExecutionTimeField extends AbstractTransitionSearchField implements DateField {
  private DatePickerConverter dateConverter;
  private final DateTimeFormatter datePickerFormatter;
  private final WorkflowUtils workflowUtils;


  public ActionExecutionTimeField(DatePickerConverter dateConverter, WorkflowManager workflowManager, DateTimeFormatterFactory dateTimeFormatterFactory, WorkflowUtils workflowUtils) {
    super(workflowManager);
    this.dateConverter = dateConverter;
    this.workflowUtils = workflowUtils;
    datePickerFormatter = dateTimeFormatterFactory.formatter().forLoggedInUser();
  }

  public String getStringFromSingularObject(Object singularObject) {
    assertObjectImplementsType(Date.class, singularObject);
    return dateConverter.getString((Date) singularObject);
  }

  public Object getSingularObjectFromString(String string) throws FieldValidationException {
    return dateConverter.getTimestamp(string);
  }

  public Object getValueFromIssue(CustomField customfield, Issue issue) {
    Collection<Long> actionIDs = getActionIDs(customfield, issue);
    if (actionIDs == null)
      return null;
    WorkflowHistoryDAO workflowHistoryDAO = useFirstExecution(customfield, issue) ?
      new WorkflowFirstHistoryDAO(true, workflowUtils) :
      new WorkflowLastHistoryDAO(true, workflowUtils);
    Map value = workflowHistoryDAO.findWorkflowEntry(issue, actionIDs);
    Timestamp finishDate = null;
    if (value.containsKey(WorkflowLastHistoryDAO.FINISHDATE_KEY))
      finishDate = (Timestamp) value.get(WorkflowLastHistoryDAO.FINISHDATE_KEY);
    return finishDate;
  }

  @Override
  public Map<String, Object> getVelocityParameters(final Issue issue, final CustomField field, final FieldLayoutItem fieldLayoutItem) {
    final Map<String, Object> map = super.getVelocityParameters(issue, field, fieldLayoutItem);
    map.put("dateTimePicker", Boolean.TRUE);
    map.put("datePickerFormatter", datePickerFormatter);
    map.put("dateFieldFormat", datePickerFormatter.withStyle(DateTimeStyle.DATE));
    map.put("titleFormatter", datePickerFormatter.withStyle(DateTimeStyle.COMPLETE));
    map.put("iso8601Formatter", datePickerFormatter.withStyle(DateTimeStyle.ISO_8601_DATE_TIME));

    return map;
  }
}
