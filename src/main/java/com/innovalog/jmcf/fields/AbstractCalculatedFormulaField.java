package com.innovalog.jmcf.fields;

import bsh.EvalError;
import bsh.Interpreter;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.util.velocity.NumberTool;
import com.googlecode.jsu.util.WorkflowUtils;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public abstract class AbstractCalculatedFormulaField<T> extends CalculatedCFType<T, T> {
  private static final String FORMULA_MARKER = "@@Formula:";
  private static GenericObjectPool interpreterPool = new GenericObjectPool(
    new BasePoolableObjectFactory() {
      @Override
      public Object makeObject() throws Exception {
        return new Interpreter();
      }
    },
    24);
  protected final Logger log = LoggerFactory.getLogger(this.getClass());
  protected final WorkflowUtils workflowUtils;

  public AbstractCalculatedFormulaField(WorkflowUtils workflowUtils) {
    this.workflowUtils = workflowUtils;
  }

  protected String className() {
    return this.getClass().getSimpleName();
  }

  protected abstract T convertResult(Object o);

  /*
      * (non-Javadoc)
      *
      * @see
      * com.atlassian.jira.issue.customfields.CustomFieldType#getValueFromIssue
      * (com.atlassian.jira.issue.fields.CustomField,
      * com.atlassian.jira.issue.Issue)
      */
  public T getValueFromIssue(CustomField field, Issue issue) {
    // get the formula from the CustomField
    String desc = field.getDescription();
    if (desc == null) {
      log.warn(className() + ": could not find formula in custom field description");
      return null;
    }
    String formula = null;
    int start = desc.indexOf("<!--");
    int end = desc.indexOf("-->", start);
    while (start >= 0 && end >= 0) {
      int formulaPos = desc.indexOf(FORMULA_MARKER, start);
      if (formulaPos >= 0 && formulaPos < end) {
        formula = desc.substring(formulaPos + FORMULA_MARKER.length(), end);
        break;
      } else {
        start = desc.indexOf("<!--", end);
        end = desc.indexOf("-->", start);
      }
    }

    if (formula == null) {
      log.warn(className() + ": could not find formula in custom field description");
      return null;
    }

    // now we have a formula, interpret it using BeanShell
    Interpreter bsh = null;
    try {
      bsh = (Interpreter) interpreterPool.borrowObject();
    } catch (Exception e) {
      log.error(className() + ": could not create BSH interpreter: ", e);
      return null;
    }
    try {
      bsh.set("issue", new IssueProxy(issue, workflowUtils));
      bsh.set("issueObject", issue);
      Object result = bsh.eval(formula);
      if (result == null)
        return null;
      else
        return convertResult(result);
    } catch (EvalError e) {
      log.error(className() + ": error evaluating formula: " + e.getMessage());
    } finally {
      if (bsh != null)
        try {
          bsh.unset("issue");
          bsh.unset("issueObject");
          interpreterPool.returnObject(bsh);
        } catch (Exception e) {
          log.error(className() + ": could not return BSH interpreter to pool: ", e);
        }
    }

    return null;
  }

}
