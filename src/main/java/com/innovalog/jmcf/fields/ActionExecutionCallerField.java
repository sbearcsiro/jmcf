package com.innovalog.jmcf.fields;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comparator.UserBestNameComparator;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.UserField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.usercompatibility.UserCompatibilityHelper;
import com.atlassian.jira.workflow.WorkflowManager;
import com.googlecode.jsu.util.WorkflowUtils;

import java.util.Collection;
import java.util.Map;

public class ActionExecutionCallerField extends AbstractTransitionSearchField implements UserField {
  private final JiraAuthenticationContext authenticationContext;
  private final WorkflowUtils workflowUtils;

  public ActionExecutionCallerField(WorkflowManager workflowManager, JiraAuthenticationContext authenticationContext, WorkflowUtils workflowUtils) {
    super(workflowManager);
    this.authenticationContext = authenticationContext;
    this.workflowUtils = workflowUtils;
  }

  public Object getSingularObjectFromString(String s) throws FieldValidationException {
    return workflowUtils.convertValueToAppUser(s);
  }

  public String getStringFromSingularObject(Object o) {
    return UserCompatibilityHelper.convertUserObject(o).getKey();
  }

  public Object getValueFromIssue(CustomField customfield, Issue issue) {
    Collection<Long> actionIDs = getActionIDs(customfield, issue);
    if (actionIDs == null)
      return null;
    WorkflowHistoryDAO workflowHistoryDAO = useFirstExecution(customfield, issue) ?
      new WorkflowFirstHistoryDAO(true, workflowUtils) :
      new WorkflowLastHistoryDAO(true, workflowUtils);
    Map value = workflowHistoryDAO.findWorkflowEntry(issue, actionIDs);
    if (value.containsKey(WorkflowLastHistoryDAO.CALLER_KEY))
      return value.get(WorkflowLastHistoryDAO.CALLER_KEY);
    else
      return null;
  }

  public int compare(final Object customFieldObjectValue1, final Object customFieldObjectValue2, final FieldConfig fieldConfig) {
    return new UserBestNameComparator(authenticationContext.getLocale()).compare((User) customFieldObjectValue1, (User) customFieldObjectValue2);
  }
}
