/**
 * 
 */
package com.innovalog.jmcf.Jql;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.api.UserWithAttributes;
import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;

/**
 * @author david
 *
 */
public class CurrentUserPropertyAsListJqlFunction extends AbstractJqlFunction {
    protected final UserPropertyManager userPropertyManager;

    public CurrentUserPropertyAsListJqlFunction(UserPropertyManager userPropertyManager) {
        this.userPropertyManager = userPropertyManager;
    }

	public MessageSet validate(User searcher, FunctionOperand operand, TerminalClause terminalClause) {
		MessageSet messages = this.validateNumberOfArgs(operand, 1);
		if (messages.hasAnyErrors())
			return messages;
		//make sure the property exists for the current user
		if (! userPropertyManager.getPropertySet(searcher).exists("jira.meta."+operand.getArgs().get(0)))
			messages.addErrorMessage(getI18n().getText("currentUserPropertyAsList.noSuchProperty",operand.getArgs().get(0)));
		return messages;
	}

	/* (non-Javadoc)
	 * @see com.atlassian.jira.plugin.jql.function.JqlFunction#getValues(com.atlassian.jira.jql.query.QueryCreationContext, com.atlassian.query.operand.FunctionOperand, com.atlassian.query.clause.TerminalClause)
	 */
	public List<QueryLiteral> getValues(QueryCreationContext queryCreationContext, FunctionOperand operand, TerminalClause terminalClause) {
        final List<String> arguments = operand.getArgs();
        //Can't do anything when no argument is specified. This is an error so return empty list.
        if (arguments.size()!=1
          || ! userPropertyManager.getPropertySet(queryCreationContext.getUser()).exists("jira.meta."+operand.getArgs().get(0)))
        {
            return Collections.emptyList();
        }

        //get the property value
        String propVal = userPropertyManager.getPropertySet(queryCreationContext.getUser())
          .getString("jira.meta."+operand.getArgs().get(0));
        String[] values = propVal.split(",");
        final List<QueryLiteral> literals = new ArrayList<QueryLiteral>(values.length);
        for (String value : values)
        {
        	if (value.length()==0)
        		literals.add(new QueryLiteral());
        	else
        		literals.add(new QueryLiteral(operand, value.trim()));
        }
        return literals;
	}

	/* (non-Javadoc)
	 * @see com.atlassian.jira.plugin.jql.function.JqlFunction#getMinimumNumberOfExpectedArguments()
	 */
	public int getMinimumNumberOfExpectedArguments() {
		return 1;
	}

	/* (non-Javadoc)
	 * @see com.atlassian.jira.plugin.jql.function.JqlFunction#getDataType()
	 */
	public JiraDataType getDataType() {
		return JiraDataTypes.ALL;
	}

}
